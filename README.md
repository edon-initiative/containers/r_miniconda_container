# About

Docker container with R, miniconda and Rstudio installed. Image is stored in Packages and regsitries > Container Registry section

Conducting some minimal testing with tools/packages we will likely use day to day.

# How to use a docker container from this repository

on your local machine run the following to clone the image:

`docker pull registry.gitlab.com/edon-initiative/containers/r_miniconda_container:latest`

To run the docker container type:

`docker run --rm -p 8787:8787 registry.gitlab.com/edon-initiative/containers/r_miniconda_container:latest`

Head to `localhost:8787` in your browser and whatever the docker image is confugured to run on port 8787 will appear - currently just as an example Rstudio will be served, but this could be configured to run Jupyter notebooks or something like that.

# Installing new tools to the Docker container

To enter the docker shell and install new tools (the container must already be running - so open a new terminal) first find the list of running containers:

`docker ps -a`

and with the image name type (usually you can press tab and it will offer a list to autocomplete):

`docker exec -it <docker-image-name> /bin/bash`

To make any changes permement exit the docker shell with `exit` and then type:

`docker commit registry.gitlab.com/edon-initiative/containers/r_miniconda_container:<new-tag-name>`

You will then have built a new image that uses the exisitng docker image with a new tag name i.e `registry.gitlab.com/edon-initiative/containers/r_miniconda_container:pycharm_install`
